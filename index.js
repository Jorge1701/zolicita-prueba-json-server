const axios = require( 'axios' );
const fs = require( 'fs' );
const jsonServer = require( 'json-server' );

function iniciarServidorJson ()
{
	const server = jsonServer.create();
	const router = jsonServer.router( 'db.json' );
	const middlewares = jsonServer.defaults();

	server.use( middlewares );
	server.use( router );
	server.listen( 3000, err => {
		if ( err )
			return console.log( 'No se pudo iniciar el servidor JSON: ' + err );
		
		console.log( 'JSON server is running' );
	} );
}

// Se obtienen los datos de la API
axios.get( 'https:\/\/randomuser.me/api/?results=200&nat=es' )
.then( res => {
	let data = { users: [] };

	// Por cada resultado obtenido de la API se agregan campos necesarios y quitan campos innecesarios
	res.data.results.forEach( ( user, index ) => {
		const newUser = user
		
		newUser.dni = user.id.value
		newUser.id = index
		newUser.name.fullname = `${user.name.title} ${user.name.first} ${user.name.last}`
		newUser.location.street = `${user.location.street.number} ${user.location.street.name}`
		newUser.dob = user.dob.date.replace( 'T', ' ' ).split( '.' )[0]
		newUser.registered = user.registered.date.replace( 'T', ' ' ).split( '.' )[0]
		newUser.picture = user.picture.large // cual uso large medium o thumbnail

		delete newUser.location.country
		delete newUser.location.coordinates
		delete newUser.location.timezone
		delete newUser.login.uuid
		delete newUser.login.md5
		delete newUser.login.sha256

		// Se agrega el nuevo usuario a los resultados
		data.users.push( newUser );
	} );

	// Se crea el archivo db.json con los datos iniciales
	fs.writeFile( 'db.json', JSON.stringify( data ), err => {
		if ( err )
			return console.log( 'No se pudieron guardar los datos de prueba.' );

		// Si no ocurrió ningún error al cargar los datos se inicia el servidor json
		iniciarServidorJson();
	} )
} )
.catch( err => console.log( 'No se pudieron descargar los datos de prueba.' ) );
